"use strict";

var Anchorscroll = (function() {

	var viewport = document.body;
	if (/Firefox/i.test(navigator.userAgent)) {
		viewport = document.getElementsByTagName("html")[0];
	}

	var init = function () {
		var link = document.getElementsByTagName("a");
		for (var i = 0; i < link.length; ++i) {
			var href = link[i].getAttribute('href');
			if(href && href.indexOf("#") === 0){
				link[i].addEventListener('click', findHref);
			}
		}
	};

	var findHref = function (link) {
		var href = document.getElementById(this.getAttribute('href').substring(1)),
			toId = href.offsetLeft - 400;
		move(toId);
		link.preventDefault();
	};

	var move = function (toId) {
		var step = function(quad) {
			viewport.scrollLeft = viewport.scrollLeft + (toId - viewport.scrollLeft) * quad;
		};
		animate(step);
	};

	var animate = function (step) {
		var start = new Date();
		var interval = setInterval(function() {
			var timePassed = new Date() - start;
			var progress = timePassed / 1200;
			step(square(progress));

			if (progress > 1) {
				clearInterval(interval);
				window.removeEventListener('click', click);
			}
		},10);

		window.setTimeout(function(){
			window.addEventListener('click', click);
		},1);

		var click = function() {
			clearInterval(interval);
			window.removeEventListener('click', click, false);
		};
	};

	var square = function(progress) {
		return Math.pow(progress, 2);
	};

	return {
		init: init
	};
})();
window.addEventListener('load',Anchorscroll.init);
