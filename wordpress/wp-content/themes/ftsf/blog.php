<?php
$post = get_post();
?>

<div class="page-head">
	<a href="back"><img src="<?= get_template_directory_uri() ?>/images/close.png" alt="Back" class="blog-page-button-back"></a>
	<h2 class="blog-page-title"><?= $post->post_title ?></h2>
</div>

<?= get_the_post_thumbnail( $post, 'post-thumbnail', array( 'class' => 'blog-page-img' ) ) ?>

<div class="blog-page-content"><?= $post->post_content ?></div>
<div class="author">
	<p class="post-author">Post author: <span class="author-name"><?= get_the_author( 'name' ); ?></span></p>
	<p class="date"><?= get_the_date( 'j F, Y' ) ?></p>
</div>
<div class="blog-page-footer">

	<?php
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
	?>

</div>
</body>
</html>
